/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.festaRave.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Ingresso {
    
    private double valor;
    private String tipo; // vip, camarote ou pista (selecionar, nao  digitar, ai a string vai ai pra dentro)
    private int codigo; //numero, para contagem, tipo ingresso 1, ingresso 2... etc

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
     public void inserir() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO Ingresso (valor, tipo, codigo) VALUES"
                + "(?,?,?)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setDouble(1, this.getValor());
            preparedStatement.setString(2, this.getTipo());
            preparedStatement.setInt(3, this.getCodigo());
            
            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Ingresso inserido no banco");
        
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
     
      public void update() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;
                                                               //1              2                   3                4
        String updateTableSQL = "update Ingresso set valor = (?), tipo = (?) where codigo = ?";
        // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            prepareStatement = dbConnection.prepareStatement(updateTableSQL);
            prepareStatement.setInt(3, this.getCodigo());
            prepareStatement.setDouble(1, this.getValor());
            prepareStatement.setString(2, this.getTipo());

            //execute insert SQL Statement
            prepareStatement.executeUpdate();
            
            System.out.println("Ingresso modificado!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
       public boolean delete() { //ok
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps = null;
        String deleteTableSQL = "DELETE FROM Ingresso WHERE codigo = ?";

        try {
            ps = dbConnection.prepareStatement(deleteTableSQL);
            ps.setInt(1, this.getCodigo());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            conexao.desconecta();
        }
        
        return true;
    }
    
    
    
}
