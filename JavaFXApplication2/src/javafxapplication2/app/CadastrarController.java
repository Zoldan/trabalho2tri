/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2.app;

import java.io.IOException;
import static java.lang.String.format;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import model.Festa;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class CadastrarController implements Initializable {

    @FXML
    private TextField dataTextField;
    @FXML
    private TextField localizacaoTextField;
    @FXML
    private TextField horarioTextField;
    @FXML
    private TextField produtorTextField;
    @FXML
    private TextField capacidadeTextField;
    @FXML
    private TextField faixaEtariaTextField;
    @FXML
    private TextField siteTextField;
    @FXML
    private TextField atracaoPrincipalTextField;
    @FXML
    private TextField trilhaSonoraTextField;
    @FXML
    private TextField descricaoTextField;
    @FXML
    private Button vipButton;
    @FXML
    private Button pistaButton;
    @FXML
    private Button camaroteButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        voltaDados();
    }
    
    static ArrayList<String> dados2 = new ArrayList();
    static HashMap<String,String> dados = new HashMap();
    
    public void cadastrar () throws ParseException{
        /*SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date data = (Date) formato.parse(dataTextField.getText()); */
      
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date data = new java.sql.Date(format.parse(dataTextField.getText()).getTime());
      
            SimpleDateFormat formatador = new SimpleDateFormat("HH:mm");
            java.sql.Time hora = new java.sql.Time(formatador.parse(horarioTextField.getText()).getTime());
     
       
        Festa f = new Festa();
        
        f.setAtracaoPrincipal(atracaoPrincipalTextField.getText());
        f.setCapacidade(Integer.parseInt(capacidadeTextField.getText()));
        f.setData(data);
        f.setDescricao(descricaoTextField.getText());
        f.setFaixaEtaria(Integer.parseInt(faixaEtariaTextField.getText()));
        f.setHorario(hora);
        f.setLocalizacao(localizacaoTextField.getText());
        f.setProdutor(produtorTextField.getText());
        f.setSite(siteTextField.getText());
        f.setTrilhaSonora(trilhaSonoraTextField.getText());
        
        f.inserir();
        
    }
    
    public void pesquisar (){
        
    }
    
    public void salvaDados (){
     
     dados.put("localizacao",localizacaoTextField.getText());
     dados.put("horario",horarioTextField.getText());
     dados.put("produtor",produtorTextField.getText());
     dados.put("capacidade",capacidadeTextField.getText());
     dados.put("faixaEtaria",faixaEtariaTextField.getText());
     dados.put("site",siteTextField.getText());
     dados.put("atracaoPrincipal",atracaoPrincipalTextField.getText());
     dados.put("trilhaSonora",trilhaSonoraTextField.getText());
     dados.put("descricao",descricaoTextField.getText());
     dados.put("data",dataTextField.getText());
     
    }
    
    public void voltaDados (){
        localizacaoTextField.setText(dados.get("localizacao"));
        horarioTextField.setText(dados.get("horario"));
        produtorTextField.setText(dados.get("produtor"));
        capacidadeTextField.setText(dados.get("capacidade"));
        faixaEtariaTextField.setText(dados.get("faixaEtaria"));
        siteTextField.setText(dados.get("site"));
        atracaoPrincipalTextField.setText(dados.get("atracaoPrincipal"));
        trilhaSonoraTextField.setText(dados.get("trilhaSonora"));
        descricaoTextField.setText(dados.get("descricao"));
        dataTextField.setText(dados.get("data"));
    }
    
    public void visualizar (){
        
    }
    public void selecionaIngresso (){
      
        try {
            salvaDados();
            Main.trocaTela("telaCadastroIngresso");
            
            // TODO
        } catch (IOException ex) {
            Logger.getLogger(TelaInicialFestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        }

    
    
}
