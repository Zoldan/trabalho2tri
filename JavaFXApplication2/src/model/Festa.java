/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;

/**
 *
 * @author Aluno
 */
public class Festa {
    
    private String localizacao; //ok
    private Time horario; //ok
    private String produtor; //ok
    private int capacidade; //ok
    private int faixaEtaria; //ok
    private String site; //ok
    private String descricao;
    private String atracaoPrincipal;
    private String trilhaSonora;
    private Date data;
    
    //select  to_char(horario,'HH24:MI:SS'), data from OO_Festa

    public int getFaixaEtaria() {
        return faixaEtaria;
    }

    public void setFaixaEtaria(int faixaEtaria) {
        this.faixaEtaria = faixaEtaria;
    }

    
    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public Time getHorario() {
        return horario;
    }

    public void setHorario(Time horario) {
        this.horario = horario;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }


    public String getProdutor() {
        return produtor;
    }

    public void setProdutor(String produtor) {
        this.produtor = produtor;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAtracaoPrincipal() {
        return atracaoPrincipal;
    }

    public void setAtracaoPrincipal(String atracaoPrincipal) {
        this.atracaoPrincipal = atracaoPrincipal;
    }

    public String getTrilhaSonora() {
        return trilhaSonora;
    }

    public void setTrilhaSonora(String trilhaSonora) {
        this.trilhaSonora = trilhaSonora;
    }
    
    public void inserir() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_Festa (idFesta, localizacao, horario, produtor, capacidade, faixaEtaria, "
                + "site, descricao, atracaoPrincipal, trilhaSonora, data) VALUES"
                + " ((select max(idFesta) from OO_Festa)+1,?,?,?,?,?,?,?,?,?,?)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.getLocalizacao());
            preparedStatement.setTime(2, this.getHorario());
            preparedStatement.setString(3, this.getProdutor());
            preparedStatement.setInt(4, this.getCapacidade());
            preparedStatement.setInt(5, this.getFaixaEtaria());
            preparedStatement.setString(6, this.getSite());
            preparedStatement.setString(7, this.getDescricao());
            preparedStatement.setString(8, this.getAtracaoPrincipal());
            preparedStatement.setString(9, this.getTrilhaSonora());
            preparedStatement.setDate(10, this.getData());
            System.out.println(this.getData());
            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Festa inserida no banco");
        
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void update() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;
                                                              
        String updateTableSQL = "update OO_Festa set localizacao = (?), horario = (?), produtor = (?)"
                + "capacidade = (?), faixaEtaria = (?), site = (?), descricao = (?), atracaoPrincipal = (?), trilhaSonora = (?) where idFesta = ?";
        // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            prepareStatement = dbConnection.prepareStatement(updateTableSQL);
           // prepareStatement.setInt(10, this.getIdFesta());
            prepareStatement.setString(1, this.getLocalizacao());
            prepareStatement.setTime(2, this.getHorario());
            prepareStatement.setString(3, this.getProdutor());
            prepareStatement.setInt(4, this.getCapacidade());
            prepareStatement.setInt(5, this.getFaixaEtaria());
            prepareStatement.setString(6, this.getSite());
            prepareStatement.setString(7, this.getDescricao());
            prepareStatement.setString(8, this.getAtracaoPrincipal());
            prepareStatement.setString(9, this.getTrilhaSonora());

            //execute insert SQL Statement
            prepareStatement.executeUpdate();
            
            System.out.println("Festa modificada!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean delete() { //ok
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps = null;
        String deleteTableSQL = "DELETE FROM OO_Festa WHERE idFesta = ?";

        try {
            ps = dbConnection.prepareStatement(deleteTableSQL);
           // ps.setInt(1, this.getIdFesta());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            conexao.desconecta();
        }
        
        return true;
    }
    
    
}
