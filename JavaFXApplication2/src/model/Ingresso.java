/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Ingresso {
    
    private double valor;
    private int idTipoIngresso;
    private int idFesta; 

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getIdTipoIngresso() {
        return idTipoIngresso;
    }

    public void setIdTipoIngresso(int idTipoIngresso) {
        this.idTipoIngresso = idTipoIngresso;
    }

    public int getIdFesta() {
        return idFesta;
    }

    public void setIdFesta(int idFesta) {
        this.idFesta = idFesta;
    }

    
    
     public void inserir() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO Ingresso (valor, idTipoIngresso, idFesta, idIngresso) VALUES"
                + "(?,?,?,(select max(idFesta) from OO_Festa)+1)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setDouble(1, this.getValor());
            preparedStatement.setInt(2, this.getIdTipoIngresso());
            preparedStatement.setInt(3, this.getIdFesta());
            
            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Ingresso inserido no banco");
        
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
     
      public void update() { // ok

        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;
                                                               //1              2                   3                4
        String updateTableSQL = "update Ingresso set valor = (?), tipo = (?) where codigo = ?";
        // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            prepareStatement = dbConnection.prepareStatement(updateTableSQL);
            //prepareStatement.setInt(3, this.getCodigo());
            prepareStatement.setDouble(1, this.getValor());
           // prepareStatement.setString(2, this.getTipo());

            //execute insert SQL Statement
            prepareStatement.executeUpdate();
            
            System.out.println("Ingresso modificado!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
       public boolean delete() { //ok
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps = null;
        String deleteTableSQL = "DELETE FROM Ingresso WHERE codigo = ?";

        try {
            ps = dbConnection.prepareStatement(deleteTableSQL);
          //  ps.setInt(1, this.getCodigo());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            conexao.desconecta();
        }
        
        return true;
    }
       public void getAll() throws SQLException {

        String selectSQL = "SELECT * FROM Ingresso";
        ArrayList<Ingresso> listaIngresso = new ArrayList<>();
        ConexaoBanco conexao = new ConexaoBanco();
        Connection dbConnection = conexao.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Ingresso ingresso = new Ingresso();
                ingresso.setValor(rs.getDouble("valor"));
              //  ingresso.setTipo(rs.getString("tipo"));
               // ingresso.setCodigo(rs.getInt("codigo"));
                listaIngresso.add(ingresso);
            }
            for (Ingresso i : listaIngresso) { // criar método no main para mostrar
                //System.out.println(i.getCodigo());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    
    
       }
        public void getOne() throws SQLException{
            String selectSQL = "SELECT * FROM Ingresso WHERE codigo = ?";
            ConexaoBanco conexaoBanco = new ConexaoBanco();
            Connection dbConnection= conexaoBanco.getConexao();
            
            PreparedStatement ps;
            try{
                ps = dbConnection.prepareStatement(selectSQL);
              //  ps.setInt(1, this.codigo);
                ResultSet rs = ps.executeQuery();
                
                if(rs.next()){
                    this.setValor(rs.getDouble("valor"));
               //     this.setTipo(rs.getString("tipo"));
                    
                }
                
            }catch(SQLException e){
            e.printStackTrace();
            }
     }
       
}
